#include <iostream>
#include <stdlib.h>
using namespace std; 

struct nodo{

       int nro;        // en este caso es un numero entero
       struct nodo *sgte;
}; 

typedef struct nodo *Tlista; 

void insertarFinal(Tlista &lista, int valor)
{

    Tlista t, p = new(struct nodo); 

    p->nro  = valor;
    p->sgte = NULL; 

    if(lista==NULL)

    {
        lista = p;
    }

    else
    {
        t = lista;

        while(t->sgte!=NULL)

        {
            t = t->sgte;
        }

        t->sgte = p;

    }
}

void reportarLista(Tlista lista)
{

     int i = 0; 

     while(lista != NULL)
     {

          cout <<' '<< i+1 <<") " << lista->nro << endl;

          lista = lista->sgte;

          i++;

     }
}
 
void eliminarElemento(Tlista &lista, int valor)

{
    Tlista q, ant;
    q = lista;
     if(lista!=NULL)
    {
        while(q!=NULL)
        {
            if(q->nro==valor)
            {
                if(q==lista)
                    lista = lista->sgte;
                else
                    ant->sgte = q->sgte;
                delete(q);
                return;
            }
            ant = q;
            q = q->sgte;
        }
    }
    else
        cout<<" Lista vacia..!";
}
 
void menu1()
{

    
    cout<<" 1. INSERTAR NUMEROS EN LA LISTA                "<<endl;
    cout<<" 2. MOSTRAR LISTADO:                  "<<endl;
    cout<<" 3. ELIMINAR VALOR DE LA LISTA:          "<<endl;
    cout<<" 4. SALIR                            "<<endl;   
	cout<<endl<<" INSERTA LA OPCION  : ";

}

 

 

/*                        Funcion Principal

---------------------------------------------------------------------*/

 

int main()
{

    Tlista lista = NULL;

    int op;     // opcion del menu

    int _dato;  // elemenento a ingresar

    int pos;    // posicion a insertar 

    system("color 0b"); 

    do

    {
        menu1();  cin>> op; 

        switch(op)
        {            

            case 1: 
                 cout<< "\n NUMERO A INSERTAR: "; cin>> _dato;

                 insertarFinal(lista, _dato );

            break;  

            
            case 2:
                 cout << "\n\n MOSTRANDO LISTA\n\n";

                 reportarLista(lista);

            break; 

            case 3:
                cout<<"\n Valor a eliminar: "; cin>> _dato; 

                eliminarElemento(lista, _dato);

            break; 
          
                    } 

        cout<<endl<<endl;

        system("pause");  system("cls");
 

    }while(op!=4);
 

   system("pause");

   return 0;

}
