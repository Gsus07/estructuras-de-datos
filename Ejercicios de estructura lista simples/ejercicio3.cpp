#include <iostream>

#include <stdlib.h>

using namespace std;

 

struct nodo{
       int num;        // en este caso es un numero entero

       struct nodo *sgte;
}; 

typedef struct nodo *Tlista; 

void insertarFinal(Tlista &lista, int valor)
{

    Tlista t, p = new(struct nodo);

    p->num  = valor;
    p->sgte = NULL; 

    if(lista==NULL)
    {

        lista = p;

    }

    else
    {

        t = lista;

        while(t->sgte!=NULL)
        {

            t = t->sgte;

        }

        t->sgte = p;
    }
}

 


void reportarLista(Tlista lista)
{

     int i = 0; 

     while(lista != NULL)
     {

          cout <<"|   ";
		  cout << lista->num << endl;

          lista = lista->sgte;

          i++;

     }
     if (lista==NULL) {
  cout<<"            lista simple vacia."; }

}

void eliminaRepetidos(Tlista &lista, int valor)
{

    Tlista p, ant;

    p = lista;

    ant = lista; 

    while(p!=NULL)
    {

        if(p->num==valor)
        {

            if(p==lista) // primero elemento
            {

                lista = lista->sgte;

                delete(p);

                p = lista;
            }

            else
            {

                ant->sgte = p->sgte;

                delete(p);

                p = ant->sgte;
            }
        }

        else
        {
            ant = p;
            
            p = p->sgte;
        } 

    }// fin del while   

    cout<<"\n\n Valores repetidos eliminados..!"<<endl;

}

 

void menu1()
{

   
    cout<<" 1. INSERTAR DATOS                "<<endl;
    cout<<" 2. MOSTRAR LA LISTA                   "<<endl;
    cout<<" 3. ELIMINAR VALOR REPETIDOS "<<endl;
    cout<<" 4. SALIR                            "<<endl;
	cout<<"\n SELECCIONE : ";
} 

/*                        Funcion Principal

---------------------------------------------------------------------*/

 

int main()
{

    Tlista lista = NULL;

    int op;     // opcion del menu

    int _dato;  // elemenento a ingresar

    int pos;    // posicion a insertar 

    system("color 09"); 

    do    
	{

        menu1();  cin>> op; 

        switch(op)
        {

            case 1:
                 cout<< "\n NUMERO A INSERTAR: "; cin>> _dato;

                 insertarFinal(lista, _dato );

            break;             

            case 2:
                  cout <<endl<<"Ver Lista Gral"<<endl;
                 reportarLista(lista);

            break;
             
            case 3:
                cout<<"\n Valor repetido a eliminar: "; cin>> _dato;

                eliminaRepetidos(lista, _dato);

            break;
                    } 

        cout<<endl<<endl;

        system("pause");  system("cls"); 

    }while(op!=4); 

   system("pause");

   return 0;

}
